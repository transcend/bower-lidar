/**
 * @ngdoc overview
 * @name transcend.lidar
 * @description
 # Lidar Module
 The "Lidar" module contains the components to query, view, and interact with Lidar data. The Lidar viewer provides the
 ability to view point data collected by remote sensing hardware.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-lidar.git
 ```

 Other options:

 *   * Download the code at [http://code.tsstools.com/bower-lidar/get/master.zip](http://code.tsstools.com/bower-lidar/get/master.zip)
 *   * View the repository at [http://code.tsstools.com/bower-lidar](http://code.tsstools.com/bower-lidar)
 *   * If you have permissions, view the source code at [http://code.tsstools.com/web-common](http://code.tsstools.com/web-common/src/ee541a3bce850ae77f6d7948a00cc352b4f67b21/modules/lidar/?at=develop)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.lidar']);
 ```

 ## To Do
 - Add unit tests.

 ## Project Goals
 - Keep this module size below 25kb minified
 */
/**
 * @ngdoc service
 * @name transcend.lidar.service:$lidar
 *
 * @description
 * The '$lidar' service contains functions for manipulation of lidar collected points.
 *
 * @requires $window
 * @requires $math
 */
/**
 * @ngdoc service
 * @name transcend.lidar:LidarData
 *
 * @description
 * The 'LidarData' factory returns different configuration parameter options for Lidar Data
 *
 * @requires $resource
 * @requires lidarConfig
 */
/**
 * @ngdoc directive
 * @name transcend.lidar.directive:lidarViewer
 *
 * @description
 The 'lidarViewer' directive provides an editable template for Lidar data.
 *
 * @restrict EA
 * @scope
 *
 * @requires $window
 * @requires $math
 * @requires $q
 * @requires transcend.core.$notify
 * @requires $lidar
 * @requires lidarConfig
 * @requires LidarData
 *
 */
